### A Pluto.jl notebook ###
# v0.18.1

using Markdown
using InteractiveUtils

# ╔═╡ 49a97c78-8b58-4654-8aa8-a9779a7deaa2
using Pkg

# ╔═╡ 16d408e2-f184-403f-adc0-ca60591ec17a
Pkg.activate("Project.toml")

# ╔═╡ 300a8b8d-dd89-4f12-96e3-96d1937c72c9
using Markdown

# ╔═╡ dc0b24fc-a0c7-4a7b-b603-2328efc6ff74
using Images

# ╔═╡ fe9345d0-3463-4ae6-be14-b13a570e2523
using ImageView

# ╔═╡ da860dc8-8254-407a-b9a6-de5e4bb5bfda
using TestImages

# ╔═╡ ebb14532-20ea-4f9c-a522-c1f205844203
using FileIO

# ╔═╡ 1e67827f-eb98-48a5-b735-a1327360a856
using PlutoUI

# ╔═╡ 05a3605a-e481-41e7-8cde-92d27c0d18e3
using Flux

# ╔═╡ 181787a3-285b-4f13-adaf-7f20101618f8
using Flux: Data.DataLoader

# ╔═╡ 293c1da4-a89f-4a21-9e5c-a880855a6eb5
using Flux: gradient

# ╔═╡ 2c4952bf-8af5-40ec-a0fe-dde8ecee7216
using Flux: @epochs

# ╔═╡ 5129d019-9ada-414b-b18c-0fd8417d8fcf
using Statistics

# ╔═╡ 715973cd-8cc6-4202-91f3-e8dd5d48c176
using Plots

# ╔═╡ 2fd62c94-541a-4ddb-aace-1a927bf75db3
using Flux: onehotbatch, onecold, crossentropy, throttle, logitcrossentropy

# ╔═╡ 7804dce4-2c42-4a59-a548-5d3e2317eabc
using InteractiveUtils

# ╔═╡ d1171da7-4750-45aa-aa78-70166b1a97dd
using Base.Iterators: repeated, partition

# ╔═╡ ef9864b5-53e0-4413-9394-2e2100002ac2
using BSON

# ╔═╡ 2c0eb57c-607f-4e39-a84f-2dcca9558a87
using Printf

# ╔═╡ a656c9a2-4072-459e-be9c-d20a519fad0a
using Parameters: @with_kw

# ╔═╡ ea4c8e60-b428-11ec-3423-81a7dd55b8b8


# ╔═╡ 7af02932-45bd-4021-acc8-4b097dea8f4f
function load_images(path::String)
    img_paths = readdir(path)
    imgs = []
    for img_path in img_paths
        push!(imgs, load(string(path,img_path)))
    end
    return imgs
end

# ╔═╡ 550173b7-47d8-4a2c-8474-222f7c1e0323
test_image1 = [load(img) for img in readdir("C:/Users/1xcarbon/Documents/ARI711S/chest_xray/test/NORMAL", join = true)]

# ╔═╡ 3dd6294e-2f50-45cf-bfa6-0d06c0cece91
test_image2 = [load(img) for img in readdir("C:/Users/1xcarbon/Documents/ARI711S/chest_xray/test/PNEUMONIA", join = true)]

# ╔═╡ ede21768-43f2-4346-be4a-9b2bfa499177
train_image2 = [load(img) for img in readdir("C:/Users/1xcarbon/Documents/ARI711S/chest_xray/train/NORMAL", join = true)]

# ╔═╡ 7efd48e1-338a-464c-9a2c-1018de7c37ef
train_normal_img = load("C:/Users/Sacky/Documents/ARI711S/chest_xray/train/NORMAL/IM-0115-0001.jpeg")

# ╔═╡ 97ab55f2-3555-46e5-97e2-f63562c26374
train_2imgs = [load(img) for img in readdir("C:/Users/1xcarbon/Documents/ARI711S/chest_xray/train/PNEUMONIA", join = true)]

# ╔═╡ 99824380-2798-4073-bd21-f21187af8a01
train_pneumonia_img = load("C:/Users/1xcarbon/Documents/ARI711S/chest_xray/train/PNEUMONIA/person1_bacteria_1.jpeg")

# ╔═╡ b4f0f8d2-2f42-4199-a57f-a69219825428
val_image1 = [load(img) for img in readdir("C:/Users/1xcarbo/Documents/ARI711S/chest_xray/val/NORMAL", join = true)]

# ╔═╡ 6e4087b1-df9a-474f-913b-9b2f44342fc9
val_image2 = [load(img) for img in readdir("C:/Users/1xcarbon/Documents/ARI711S/chest_xray/val/PNEUMONIA", join = true)]

# ╔═╡ 2cc81e1a-cd01-4d6b-8a5d-72f382b28c29
size(test_image1)

# ╔═╡ 3f605167-fa4b-45eb-a3cd-e9b8f322dcf0
size(test_image2)

# ╔═╡ f1087c03-7dda-402a-a509-c821001c8b21
size(train_image1)

# ╔═╡ a7e86e21-4e6d-4ee8-9af5-e392eb827312
size(train_2imgs)

# ╔═╡ f45d729d-5361-403a-b134-e7b646de4722
size(val_image1)

# ╔═╡ 38a8168f-06bd-4b93-a151-078eb81358e0
size(val_image2)

# ╔═╡ acc4158d-7df6-42fb-a930-8537eb2ac0bb
df(x) = gradient(f, x)[1]

# ╔═╡ 907a88aa-adf9-487a-b3e4-2e5c08fc0c93
df(5)

# ╔═╡ 98c2511d-c050-4c42-92f8-85d51d55f880
myloss(W, b, x) = sum(W * x .+ b)

# ╔═╡ 5de76bd7-707f-45a9-84a5-8dc5bc99e516
m = Dense(10, 5)

# ╔═╡ 120432f3-397c-4ea1-9b14-e3536c6da640
function make_batch(X, Y, idxs)
    Xbatch = Array{Float32}(undef, size(X[1])..., 1, length(idxs))
    for i in 1:length(idxs)
        Xbatch[:, :, :, i] = Float32.(X[idxs[i]])
    end
	
    Ybatch = onehotbatch(Y[idxs], 0:9)
    return (Xbatch, Ybatch)
end

# ╔═╡ 8d8d2a43-ec5b-43af-9fd5-475c31c100d1
Array{Tuple{Array{Float32,4},Flux.OneHotMatrix{Array{Flux.OneHotVector,1}}},1}

# ╔═╡ 4b261348-df95-43d0-a990-07598aa2b38a
model = Chain(
    Conv((3, 3), 1=>16, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),
    Conv((3, 3), 16=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),
    Conv((3, 3), 32=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),
	Conv((3, 3), 32=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),
	Conv((3, 3), 32=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),
    x -> reshape(x, :, size(x, 4)),
    Dense(288, 10),
    softmax,
)

# ╔═╡ 7e62d40f-b1e7-44f6-96ab-04db57b17047
size(train_set[1][1]) # Float32

# ╔═╡ b4f03f16-6f72-43dd-9804-7104c0c8ab55
size(train_set[1][2])

# ╔═╡ 0e14ee0a-bb8e-4551-aec2-7db5bcc11c3b
train_set[1][2][:,1]

# ╔═╡ dc0e4f9a-4383-4479-91f5-4fc64cfdbc0b
model(train_set[1][1])

# ╔═╡ 560bc930-4b67-437a-8823-0c3b012c7920
function loss(x, y)
    x_aug = x .+ 0.1f0*gpu(randn(eltype(x), size(x)))

    y_hat = model(x_aug)
    return crossentropy(y_hat, y)
end

# ╔═╡ ecbaf6c3-9fb0-47d1-aa42-5bc78f34fb9b
accuracy(x, y) = mean(onecold(model(x)) .== onecold(y))

# ╔═╡ 04fa23e6-a10d-4c64-9102-5978384dc229
opt = ADAM(0.001)

# ╔═╡ a8961f8e-cf13-4494-a62c-70d9da80d67d
begin
	augment(x) = x .+ gpu(0.1f0*randn(eltype(x), size(x)))
    paramvec(m) = hcat(map(p->reshape(p, :), params(m))...)    
    anynan(x) = any(isnan.(x))
    accuracy(x, y, model) = mean(onecold(cpu(model(x))) .== onecold(cpu(y)))
end

# ╔═╡ e0e87368-e2a2-404e-a286-5eb6f4d0dc6a
begin
	best_acc = 0.0
    last_improvement = 0
    for epoch_idx in 1:100
		global best_acc, last_improvement        
        Flux.train!(loss, params(model), train_set, opt)        
        acc = accuracy(test_set...)    
        if acc >= 0.999
      end
        if acc >= best_acc
        @info("Saving model out to model.bson")
        BSON.@save "model.bson" model epoch_idx acc
        best_acc = acc
        last_improvement = epoch_idx
        end
        if epoch_idx - last_improvement >= 5 && opt.eta > 1e-6
			opt.eta /= 10.0    
        last_improvement = epoch_idx
        end
        if epoch_idx - last_improvement >= 10        
		end
    end
end

# ╔═╡ ac9eb872-23b3-4264-a65a-2bc4ac6feccd
function sigmoid(z)
1 ./ (1 .+ exp.(.-z))
end

# ╔═╡ 44d4f348-032c-4cb4-9972-3a09f2907fbc
function transform_features(xMatrix, u, o)
       XNorm = (xMatrix .- u) ./ o
       return X_norm
       end

# ╔═╡ e5f4aa4f-5155-46f4-880e-7e6c851da656
begin
	W = randn(3, 5)
    b = zeros(3)
    x = rand(5)
end

# ╔═╡ 9706f75f-e698-486a-aa41-b0a8a6f9e391
gradient(myloss, W, b, x)

# ╔═╡ 35230e38-8f49-4a93-97be-ed0076b0fe0a
begin
	train_set = gpu.(train_set)
    test_set = gpu.(test_set)
    modl = gpu(model)
end

# ╔═╡ 6998e512-bd4d-4e9b-af63-7df9807ff5b6

begin
	test_set = make_minibatch(test_imgs, test_labels, 1:length(test_imgs))
	typeof(train_set)
end

# ╔═╡ 36c24522-4e8f-4b49-b23c-8f41378a6ea8
begin
	batch_size = 128
    mb_idxs = partition(1:length(train_imgs), batch_size)
    train_set = [make_minibatch(train_imgs, train_labels, i) for i in mb_idxs]
end

# ╔═╡ Cell order:
# ╠═ea4c8e60-b428-11ec-3423-81a7dd55b8b8
# ╠═300a8b8d-dd89-4f12-96e3-96d1937c72c9
# ╠═49a97c78-8b58-4654-8aa8-a9779a7deaa2
# ╠═16d408e2-f184-403f-adc0-ca60591ec17a
# ╠═dc0b24fc-a0c7-4a7b-b603-2328efc6ff74
# ╠═fe9345d0-3463-4ae6-be14-b13a570e2523
# ╠═da860dc8-8254-407a-b9a6-de5e4bb5bfda
# ╠═ebb14532-20ea-4f9c-a522-c1f205844203
# ╠═1e67827f-eb98-48a5-b735-a1327360a856
# ╠═05a3605a-e481-41e7-8cde-92d27c0d18e3
# ╠═181787a3-285b-4f13-adaf-7f20101618f8
# ╠═293c1da4-a89f-4a21-9e5c-a880855a6eb5
# ╠═2c4952bf-8af5-40ec-a0fe-dde8ecee7216
# ╠═5129d019-9ada-414b-b18c-0fd8417d8fcf
# ╠═715973cd-8cc6-4202-91f3-e8dd5d48c176
# ╠═2fd62c94-541a-4ddb-aace-1a927bf75db3
# ╠═7804dce4-2c42-4a59-a548-5d3e2317eabc
# ╠═d1171da7-4750-45aa-aa78-70166b1a97dd
# ╠═ef9864b5-53e0-4413-9394-2e2100002ac2
# ╠═2c0eb57c-607f-4e39-a84f-2dcca9558a87
# ╠═a656c9a2-4072-459e-be9c-d20a519fad0a
# ╠═7af02932-45bd-4021-acc8-4b097dea8f4f
# ╠═550173b7-47d8-4a2c-8474-222f7c1e0323
# ╠═3dd6294e-2f50-45cf-bfa6-0d06c0cece91
# ╠═ede21768-43f2-4346-be4a-9b2bfa499177
# ╠═7efd48e1-338a-464c-9a2c-1018de7c37ef
# ╠═97ab55f2-3555-46e5-97e2-f63562c26374
# ╠═99824380-2798-4073-bd21-f21187af8a01
# ╠═b4f0f8d2-2f42-4199-a57f-a69219825428
# ╠═6e4087b1-df9a-474f-913b-9b2f44342fc9
# ╠═2cc81e1a-cd01-4d6b-8a5d-72f382b28c29
# ╠═3f605167-fa4b-45eb-a3cd-e9b8f322dcf0
# ╠═f1087c03-7dda-402a-a509-c821001c8b21
# ╠═a7e86e21-4e6d-4ee8-9af5-e392eb827312
# ╠═f45d729d-5361-403a-b134-e7b646de4722
# ╠═38a8168f-06bd-4b93-a151-078eb81358e0
# ╠═acc4158d-7df6-42fb-a930-8537eb2ac0bb
# ╠═907a88aa-adf9-487a-b3e4-2e5c08fc0c93
# ╠═98c2511d-c050-4c42-92f8-85d51d55f880
# ╠═5de76bd7-707f-45a9-84a5-8dc5bc99e516
# ╠═9706f75f-e698-486a-aa41-b0a8a6f9e391
# ╠═120432f3-397c-4ea1-9b14-e3536c6da640
# ╠═36c24522-4e8f-4b49-b23c-8f41378a6ea8
# ╠═6998e512-bd4d-4e9b-af63-7df9807ff5b6
# ╠═8d8d2a43-ec5b-43af-9fd5-475c31c100d1
# ╠═7e62d40f-b1e7-44f6-96ab-04db57b17047
# ╠═b4f03f16-6f72-43dd-9804-7104c0c8ab55
# ╠═0e14ee0a-bb8e-4551-aec2-7db5bcc11c3b
# ╠═4b261348-df95-43d0-a990-07598aa2b38a
# ╠═35230e38-8f49-4a93-97be-ed0076b0fe0a
# ╠═dc0e4f9a-4383-4479-91f5-4fc64cfdbc0b
# ╠═560bc930-4b67-437a-8823-0c3b012c7920
# ╠═ecbaf6c3-9fb0-47d1-aa42-5bc78f34fb9b
# ╠═04fa23e6-a10d-4c64-9102-5978384dc229
# ╠═e0e87368-e2a2-404e-a286-5eb6f4d0dc6a
# ╠═a8961f8e-cf13-4494-a62c-70d9da80d67d
# ╠═ac9eb872-23b3-4264-a65a-2bc4ac6feccd
# ╠═44d4f348-032c-4cb4-9972-3a09f2907fbc
# ╠═e5f4aa4f-5155-46f4-880e-7e6c851da656
